#!/bin/bash
# run this while testing Secure Stage features to detect and sniff incoming and outgoing traffic from external resources.
sudo ngrep -l -q 'gitlab.com|registry.gitlab.com|hub.docker.com'
