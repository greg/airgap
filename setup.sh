#!/bin/bash
sudo apt-get update
## sudo apt-get upgrade -y
# install system dependencies
sudo apt-get install -y curl ca-certificates openssh-server
# add gitlab sources
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
# install gitlab: https://about.gitlab.com/install/
sudo EXTERNAL_URL="http://$(curl icanhazip.com)" apt-get install gitlab-ee
# install docker: https://docs.gitlab.com/runner/install/docker.html#docker-image-installation
curl -sSL https://get.docker.com/ | sudo bash
# sudo usermod -aG docker $(whoami)
# registry: https://docs.docker.com/registry/#basic-commands
docker run -d -p 5000:5000 --name registry registry:2
# install & start gitlab-runner: https://docs.gitlab.com/runner/install/docker.html
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest