#!/bin/bash
set -ux
analyzers=(sast dast)
gitlab=registry.gitlab.com/gitlab-org/security-products/
registry=localhost:5000
version=12-8-stable
for i in "${analyzers[@]}"
do
  echo pulling $gitlab$i:$version
  docker pull $gitlab$i:$version
  docker tag $(sudo docker images | grep $i | awk '{print $3}') $registry/analyzers/$i:$version;
  docker push $registry/analyzers/$i;
done
